<?php

	/* Disable the Admin Bar. */
	remove_action( 'init', 'wp_admin_bar_init' );
	
	/* Custom background for body */
	add_theme_support('custom-background', $args);

	/* Load CSS */
	function resources()
	{
		wp_enqueue_style('style', get_stylesheet_uri());
		wp_enqueue_script('jquery', "http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js");
		wp_enqueue_script('jquery', "https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js");
		//wp_enqueue_script('active_tab', get_template_directory_uri() . '/js/active_tab.js', array('jquery'), '1.0');
	}
	
	add_action('wp_enqueue_scripts', 'resources');
	
	/* Linkbar menu */
	function register_my_menu()
	{
		register_nav_menu('link-menu', __( 'Link Menu' ) );
		register_nav_menu('site-navigation', __( 'Site Navigation' ) );
	}
	
	add_action( 'init', 'register_my_menu' );
	
	
	
	/* Theme options */
	function theme_customizer( $wp_customize )
	{
		/*  Logo */
		setLogo($wp_customize);
								
		/* Login */
		setLogin($wp_customize);
	}
	
	function setLogo($wp_customize)
	{
		/* Section */
		$wp_customize->add_section('logo_section', array(
			'title' 	  => __('Site Logo'),
			'priority'    => 100
			));
			
		/* Setting */
		$wp_customize->add_setting('logo_setting');
		
		/*  Control */
		$wp_customize->add_control(new WP_Customize_Image_Control(
									$wp_customize, 'logo_setting',
									array(
									'label' => __('Logo'),
									'section' => 'logo_section',
									'settings' => 'logo_setting'
									)));
	}
	
	function setLogin($wp_customize)
	{
		$wp_customize->add_section('login_section', array(
		'title' 	  => __('Site Login'),
		'priority'    => 90
		));
		
		/* Setting */
		$wp_customize->add_setting('login_setting');
		
		/*  Control */
		$wp_customize->add_control
		(
			new WP_Customize_Control
			(
				$wp_customize,
				'login_setting',
				array
				(
					'label'          => __( 'Login form'),
					'section'        => 'login_section',
					'settings'       => 'login_setting',
					'type'           => 'checkbox'
            	)
        	)
    	);
		
		$wp_customize->add_setting('login_action');
		
		$wp_customize->add_control
		(
			new WP_Customize_Control
			(
				$wp_customize,
				'login_action',
				array
				(
					'label'		=> __('Action path'),
					'section'        => 'login_section',
					'settings'       => 'login_action',
				)
			)
		);
	}
	
	add_action('customize_register', 'theme_customizer');
	
	/* Share button options */
	function jptweak_remove_share()
	{
		remove_filter( 'the_content', 'sharing_display',19 );
		remove_filter( 'the_excerpt', 'sharing_display',19 );
		
		if ( class_exists( 'Jetpack_Likes' ) )
		{
			remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
		}
	}
	 
	add_action( 'loop_start', 'jptweak_remove_share' );
?>