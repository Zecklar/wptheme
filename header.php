<!DOCTYPE html>
<html <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo('charset'); ?> >
		<meta name="viewport" content="width=device-width">
		<title><?php bloginfo('name'); ?></title>
		<?php wp_head(); ?>
	</head>
	
<body <?php body_class( $class ); ?>> 
	
    <div id="linkbar_container">
    	<div id="linkbar">
        	
            <?php if ( has_nav_menu( 'link-menu' ) ) : ?>
			<nav class="link-navigation" role="navigation">
				<?php
					
					// Call link menu
					wp_nav_menu( array('theme_location' => 'link-menu' ) );
				?>
			</nav><!-- link navigation -->
		<?php endif; ?>
            
        </div>
    </div>
    
    <!-- Banner -->
    <header id="banner" role="banner">
        <div>
        
       	 	<!-- Logo -->
        	<div id="logo">
				<?php if( get_theme_mod( 'logo_setting' ) ) : ?>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <img src="<?php echo esc_url( get_theme_mod( 'logo_setting' ) ); ?>" alt="Logo" id="logo">
                </a>     
                <?php else : ?>
                    <h1 class="site-title"><?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?></h1>
                <?php endif ?>
            </div>
            <!-- Logo end -->
            
            <!-- Login -->
            <div class="login">
            	
					<?php if( get_theme_mod( 'login_setting' ) ) : ?>
                        <div id="loginwrapper">
                            <form action="<?php echo get_theme_mod('login_action') ?>" method="POST">
                                <input type=text" name="username" value placeholder="Username">
                                <input type="text" name="password" value placeholder="Password">
                                <input type="submit" value=">>">
                            </form>
                        </div>    
                    <?php endif ?>
                
            </div>
            <!-- Login end -->
            
            <!-- Sharing -->
            <div class="share">
                <?php
                    if ( function_exists( 'sharing_display' ) )
                    {
                        sharing_display( '', true );
                    }
                     
                    if ( class_exists( 'Jetpack_Likes' ) ) {
                        $custom_likes = new Jetpack_Likes;
                        echo $custom_likes->post_likes( '' );
                    }
                ?>
            </div>
            <!-- Sharing end -->
            
        </div>
        
    </header>
    <!-- Banner end -->
    
    <!-- Site Navigation bar -->
    <div class="navigation-container"> 
        <?php if ( has_nav_menu( 'site-navigation' ) ) : ?>
        <nav id="site-navigation" role="navigation">
            <?php
                // Call site navigation bar
                wp_nav_menu( array('theme_location' => 'site-navigation' ) );
            ?>
        </nav>
        <?php endif; ?> 
    </div>
    <!-- Site Navigation bar end -->
    
    <!-- Site content -->
    <div id="container">
    