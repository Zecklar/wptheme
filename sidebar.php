<?php
if ( has_nav_menu( 'header-menu' )) : ?>
	<div id="secondary" class="secondary">

		<?php if ( has_nav_menu( 'header-menu' ) ) : ?>
			<nav id="site-navigation" class="main-navigation" role="navigation">
				<?php
					// Primary navigation menu.
					wp_nav_menu( array(
						'theme_location' => 'header-menu'
					) );
				?>
			</nav><!-- .main-navigation -->
		<?php endif; ?>

	</div><!-- .secondary -->

<?php endif; ?>